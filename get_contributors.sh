#!/bin/bash
# It only parse the Translator header, which does not appear to be edited when using the transifex-client

POS_TMP=files_tmp
TRANSLATORS_tmp=translators_tmp
TRANSLATORS=translators

find . -name *.po > $POS_TMP


for i in `cat $POS_TMP`
do
  grep "^# .*@.*2013$" $i | sed -n "s/^# \(.*\), .*/\1/p" >> $TRANSLATORS_tmp
#  grep "^\"Last-Translator:" $i | sed -n "s/\"Last-Translator: \(\w* <.*@.*>\).*/\1/p"  >> $TRANSLATORS_tmp
done
rm $POS_TMP

sort $TRANSLATORS_tmp | uniq -c | sort -nr > $TRANSLATORS
rm $TRANSLATORS_tmp

exit 0

