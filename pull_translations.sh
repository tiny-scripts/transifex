#!/bin/bash

curr=`pwd`
proj_tmp=proj
tmp_proj=projects
MY_LANG=fr

TX_CFG_PATH=.tx
TX_CFG_FILE=$TX_CFG_PATH/config

# Test if transifex client is installed
type tx > /dev/null
if [ $? -gt 0 ]
then
	echo Please install transifex-client first
	exit 1
fi

# Do we need to init the git tree?
git branch > /dev/null
if [ $? -gt 0 ]
then
	git init
fi

# Clean the repo
/usr/bin/git clean -q -fd || exit 1
/usr/bin/git reset -q --hard || exit 1
/usr/bin/git checkout -q master || exit 1
/usr/bin/git pull -q --ff-only || exit 1


# parse all subfolders to pull from TX!
# Mind that we don't pull only reviewed ones
find . -name .tx | sed -n 's#\(.*\)/\.tx#\1#p' > $tmp_proj

lines_n=$(wc -l < $tmp_proj)
if [ $lines_n -lt 1 ]
then
    echo No project found, you probably miss the --init part…
    exit 1
fi

for i in `cat $tmp_proj`
do
    cd $i
    tx pull -l $MY_LANG
    cd $curr
done
rm $tmp_proj

# Now we get the credits for the commit message
# The following is partly taken from 
# https://github.com/AlD/Quassel/blob/tx-sync/po/pull-from-transifex.sh
translators=$(while read mode pofile; do
   lines=`git diff --numstat $pofile 2>&1 | awk '{print $1}'` 
   #if no new string found at least the build time could change (i.e. check if > 1)
   let "$((++lines))"     # cause in case the file is exactly the same, prevent no git diff output
   if [ $lines -gt 2 ]
   then                # POT should be uploaded
     translator=$(git diff $pofile | perl -le '
          while (<>) {
            if (/^(?:\+(?:#|.*?:)|[ +]"Last-Translator:) *(.*?)(<[^@>]+@[^>]+>)/p) {
              $xltrs{$2} = $1 unless $xltrs{$2};
              last if $& =~ /Last-Translator:/;
            }
          }
          push(@out, $n.$e) while (($e, $n) = each %xltrs);
          print(join(", ", @out));
     ')
     file=`echo $pofile | sed 's|^.*\/translations\/\(.*\)\/.*.po| =@= \1|g'`
     echo "$file: $translator"
     git add $pofile
  fi
done < <(git status --porcelain | egrep '^ ?[AM] '))
# TODO: Should handle untracked files
#done < <(git status --porcelain | egrep '^ ?[AM]|\?{2} '))


credit=`echo $translators | sed -e "s/=@= /\n - /g" -e  "s/@[^>]\+>/>/g"`
git commit -m "All $MY_LANG translations pulled. Summary:  
$credit"

git push

exit 0
