#!/bin/bash

while read file
do

  resource=${file%.xml}
  resource=${resource#en-US/}

  tx set `echo "--auto-local --execute -r fedora-release-notes.$resource '<lang>/$resource.po' --source-lang en --source-file pot/$resource.pot -t PO"`

done < <(ls en-US/*.xml)

  tx set -t PO fedora-release-notes
