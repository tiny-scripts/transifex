#!/usr/bin/python
# This script read a file which contains some transifex.net projects.
# For each project, it returns the language code which have to be merged

try:
    import ConfigParser
except ImportError:
    print "ConfigParser module not found"

import pycurl, json, cStringIO, time, calendar, fileinput
import argparse, re, os
from os.path import expanduser
from txlib.http.http_requests import HttpRequest
from txlib.http.auth import BasicAuth
from txlib.registry import registry
from txlib.api.translations import Translation
from txlib.api.project import Project
from txlib.api.resources import Resource

# Global variables
API_URL="https://www.transifex.net/api/2/project/"
PROJECT_URL="https://www.transifex.net/projects/p/"
ERR_M=" [EE] "

# lang_map taken from (value:key mirrored, and '-' changed by '_')
# https://fedoraproject.org/wiki/Setting_up_a_document_with_Transifex#Step_5:_Map_Language_Codes
lang_map = {'aln_AL':'aln', 'ar_SA':'ar', 'ast_ES':'ast', 'as_IN':'as',
            'bal_PK':'bal', 'bg_BG':'bg', 'bn_BD':'bn', 'bn_IN':'bn_IN',
            'bs_BA':'bs', 'ca_ES':'ca', 'cs_CZ':'cs', 'da_DK':'da',
            'de_DE':'de', 'el_GR':'el', 'en_GB':'en_GB', 'es_ES':'es', 'et_EE':'et',
            'eu_ES':'eu', 'fa_IR':'fa', 'fi_FI':'fi', 'fr_FR':'fr', 'gl_ES':'gl',
            'gu_IN':'gu', 'he_IL':'he', 'hi_IN':'hi', 'hr_HR':'hr', 'hu_HU':'hu',
            'id_ID':'id', 'is_IS':'is', 'it_IT':'it', 'ja_JP':'ja', 'kn_IN':'kn',
            'ko_KR':'ko', 'lt_LT':'lt', 'lv_LV':'lv', 'mai_IN':'mai', 'ml_IN':'ml',
            'mr_IN':'mr', 'ms_MY':'ms', 'nb_NO':'nb', 'nds_DE':'nds', 'nl_NL':'nl',
            'nn_NO':'nn', 'or_IN':'or', 'pa_IN':'pa', 'pl_PL':'pl', 'pt_BR':'pt_BR',
            'pt_PT':'pt', 'ro_RO':'ro', 'ru_RU':'ru', 'si_LK':'si', 'sk_SK':'sk',
            'sl_SI':'sl', 'sq_AL':'sq', 'sr_RS':'sr', 'sr_Latn_RS':'sr@latin',
            'sv_SE':'sv', 'ta_IN':'ta', 'te_IN':'te', 'tg_TJ':'tg', 'tr_TR':'tr',
            'uk_UA':'uk', 'ur_PK':'ur', 'vi_VN':'vi', 'zh_CN':'zh_CN',
            'zh_HK':'zh_HK', 'zh_TW':'zh_TW'}

''' Get Transifex username and password '''
config = ConfigParser.RawConfigParser()
config.read(expanduser('~')+'/.transifexrc')
txuser = config.get('https://www.transifex.net', 'username')
txpass = config.get('https://www.transifex.net', 'password')

'''create tx connection'''
txauth = BasicAuth(username=txuser, password=txpass)
txconn = HttpRequest('fedora.transifex.net', auth=txauth)
registry.setup({'http_handler': txconn})

''' return the json data from the url'''
def getJson(url):
    result = cStringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(pycurl.URL, API_URL + url)
    c.setopt(pycurl.HTTPHEADER, ['Accept: application/json'])
    c.setopt(pycurl.USERPWD, txuser + ":" + txpass)
    c.setopt(pycurl.WRITEFUNCTION, result.write)
    c.perform()
    c.close()
    return result.getvalue()

''' Checks for the project '''
def checkProject(project):
    print " === " + project
    '''lang_country:lang #translated\t#Completion\t Name of the resource to merge'''

    data = getJson(project + "/?details")
    content = json.loads(data)

    source_lang = content["source_language_code"]

    if len(content["maintainers"]) < 1:
        print ERR_M + content['slug'] + ":  Error, this project does not have any maintainer, please report!"
        print "    Check this project at " + PROJECT_URL + content['slug']
        return

    to_merge = {}
    need_action = False
    to_remove = []
    dunno = []

    ''' Parse all resources of the actual project '''
    for i in content["resources"]:
        url = project + "/resource/" + i['slug'] + "/stats/"
        data = getJson(url.encode('ascii','ignore'))
        resource_content = json.loads(data)

        ''' Parse all language code in the actual resource '''
        ''' Add the language code to the list              '''
        '''    if not in the project teams                 '''
        '''    if not the language source                  '''
        '''    if percentage of completion is greater than '''
        '''       with the correspding team (if found)     '''
        '''    if not already catched                      '''
        for lang in resource_content.keys():
            ''' Check if we gave some language code as argument '''
            if args.lang and not lang in args.lang:
                continue

            ''' If the language has already been parsed and detected as needed to
                merge, we check if we have to force parsing all resources. '''
            if lang in to_merge.keys() and args.all == False:
                continue

            if not lang in content["teams"]:
                if resource_content[lang]['translated_entities'] > 0:
                    merge_lang=''

                    if lang in lang_map:
                        merge_lang = lang_map[lang]
                    else:
                        if lang not in dunno:
                            dunno.append(lang)
                        continue

                    if merge_lang in content["teams"]:
                        lang_completion = resource_content[lang]['translated_entities']
                        try:
                            merge_lang_completion = resource_content[merge_lang]['translated_entities']
                        except KeyError:
                            print 'Error `' + merge_lang + '\' does not exist in the resource ' + i['slug']
                            continue
                        if lang_completion > merge_lang_completion:
                            ''' The  printed percentage is only about one slug (i.e. file)
                                against the project, not the whole translation stats'''
                            need_action = True
                            if args.verbose:
                                print " " + lang + ":" + merge_lang,
                                print "  " + str(lang_completion) + ">" + str(merge_lang_completion),
                                print "\t" + resource_content[lang]['completed'] + " Vs " + resource_content[merge_lang]['completed'],
                                print "\t" + i['slug']

                            try:
                                to_merge[lang].append(i['slug'])
                            except KeyError:
                                to_merge[lang] = [i['slug']]

                            if args.merge == True and not os.access(pdir + '/' + lang + '/' + i['slug'] + '.po', os.F_OK):
                                    os.system('/usr/bin/tx -r ' + pdir + ' pull -l ' + lang + ' -r ' + project + '.' + i['slug'])
                        else:
                            if lang not in to_remove and lang not in to_merge:
                                # TODO: bug report: we can have the same
                                # language code in to_merge and
                                # to_remove
                                to_remove.append(lang)
                                need_action = True
    if need_action:
        if dunno:
            try:
                dunno.remove(source_lang)
            except ValueError:
                pass
            print " - We haven't checked the following teams: ",
            for i in dunno:
                print i ,
            print ''

        if to_merge:
            print " - Team to merge: ",
            for i in sorted(to_merge.keys()):
                print i,
            print ''
        if to_remove:
            print " - Team you can already remove (please manually check): ",
            for i in sorted(to_remove):
                print i,
            print ''

        print " - Maintainer(s): ",
        for i in content["maintainers"]:
            print i["username"],

        print "\n - See project at: " + PROJECT_URL + project

        if args.merge == True:
            print '\n Trying to merge'
            '''Resources pulled. Now, we have to adapt code language mapping then push\nProcessing...'''
            for lang in to_merge.keys():
                final_lang = re.sub(r'_..', '', lang) # good lang on transifex
                goodlang = ':' + re.sub(r'_', '-', lang) # str used to replace wronglang in .tx/config file used by publican
                wronglang = ':' + lang # str used to find and replace goodlang in .tx/config file
                try:
                    txconfFileDescr = open(txconfigPath)
                    txconfContent = txconfFileDescr.readlines()
                    txconfFileDescr.close()
                    txconfFileDescr = open(txconfigPath, 'w')
                    for line in txconfContent:
                        if goodlang in line:
                            goodline =  line.replace(goodlang, wronglang, 1) # replace the good one with wrong one to be able to push resource
                            txconfFileDescr.write(goodline)
                        else:
                            txconfFileDescr.write(line)
                    txconfFileDescr.close()
                except IOError:
                    print(IOError.errno)

                for res in to_merge[lang]:
                    os.system('/usr/bin/tx -r ' + pdir + ' push -t -l ' + final_lang + ' -r ' + project + '.' + res)
                #Restoring code language mapping
                try:
                    txconfFileDescr = open(txconfigPath, 'w')
                    for line in txconfContent:
                        txconfFileDescr.write(line)
                    txconfFileDescr.close()
                except IOError:
                    print(IOError.errno)

        print ""

if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='Check transifex projects and return which teams have to be merged.')
    parser.add_argument('-a', '--all', action='store_true', help='Parse all resources, does not stops at the first one. Default False, this is *really* slow!')
    parser.add_argument('-v', '--verbose', action='store_true', help='Print the resources name')
    parser.add_argument('-l', '--lang', action='store', nargs='+', help='Parse only the provided language codes')
    parser.add_argument('-m', '--merge', action='store_true', help='Auto merge resources')

    args = parser.parse_args()

    wd = os.getcwd() + '/'
    for line in fileinput.input('projects'):
        pdir = wd + re.sub(r'^fedora-(.*)\n$',r'\1/', line)
        txconfigPath = pdir + '.tx/config'

        checkProject(line.strip())
        print ""


