#!/usr/bin/python

try:
    import ConfigParser
except ImportError:
    print "ConfigParser module not found"

import pycurl, json, cStringIO, time, calendar, fileinput
import os
from os.path import expanduser


API_URL="https://www.transifex.com/api/2/project/"
PROJECT_URL="https://www.transifex.com/projects/p/"
PROJ_NAME='projects'

''' Get Transifex username and password '''
config = ConfigParser.RawConfigParser()
config.read(expanduser('~')+'/.transifexrc')
txuser = config.get('https://www.transifex.com', 'username')
txpass = config.get('https://www.transifex.com', 'password')

default_maintainers = ['diegobz', 'raven', 'glezos']
ERR_M="M" # is printed if it seems to be a default of maintainer
ERR_DATE="D" # is printed if the ressource is old (not updated nice TIMEOUT
today_sec=time.time()
''' Define the time (is second) after which we consider the project to be dead. default=7257600, 3 months'''
TIMEOUT=12096000

''' return the json data from the url'''
def getJson(url):
    result = cStringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(pycurl.URL, API_URL + url)
    c.setopt(pycurl.HTTPHEADER, ['Accept: application/json'])
    c.setopt(pycurl.USERPWD, txuser + ":" + txpass)
    c.setopt(pycurl.WRITEFUNCTION, result.write)
    c.perform()
    c.close()
    return result.getvalue()


''' Checks for the project if
 - It has a owner
 - It has other maintainers than the one specified above, else print the [M] error
 - It has at least one resource where its language source has been updated in TIMEOUT (4 months by now)
'''
def checkProject(project):
    data = getJson(project + "/?details")
    content = json.loads(data)

    maintainers = []
    for i in content["maintainers"]:
        maintainers.append(i["username"])

    ''' Removing default maintainers, (member of the FLSCo). There are few chances for them to really be the only maintainer'''
    for i in default_maintainers:
        try:
            maintainers.remove(i)
        except:
            pass

    if len(maintainers) < 1:
        ''' The maintainer list should not contain only the default one'''
        print "[" + ERR_M + "] " + content["slug"]
        if 'owner' in content:
            print "\tproject owner = " + content["owner"]["username"]
        print '\t' + PROJECT_URL + content["slug"]
        return

    ''' Check the last source file update for each resources'''
    updated = False;
    last_update = 0

    for i in content["resources"]:
        url = project + "/resource/" + i["slug"] + "/stats/" + content["source_language_code"] + "/"
        data = getJson(url.encode('ascii','ignore'))
        resource_content = json.loads(data)
        tuple_time = time.strptime(resource_content["last_update"], "%Y-%m-%d %H:%M:%S")

        ''' record the last update (push) '''
        if calendar.timegm(tuple_time) > last_update:
            last_update = calendar.timegm(tuple_time)

        if last_update + TIMEOUT > today_sec:
            updated = True

    ''' At least one resource updated in time? '''
    if updated is False:
        print "[" + ERR_DATE + "] " + content["slug"]
        print '\tLast update on ' + time.asctime(time.gmtime(last_update))
        print '\t' + PROJECT_URL + content["slug"]


if __name__ == "__main__":
    for line in fileinput.input(PROJ_NAME):
        checkProject(line.strip())

