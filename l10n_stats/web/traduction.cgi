#!/usr/bin/python
# -*- coding: utf-8 -*-

import time, os, sys, filecmp
from time import strftime, localtime, strptime
import calendar
import json
import urllib

METADATA_URL = 'https://wip.fedora-fr.org/p/traduction/source/file/master/metadata'
METADATA = 'metadata'
TIMEOUT = 3600 # 1h
PRIORITY = 'priority.json'

html_header=u'''<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="style.css"/>
      <link rel="shortcut icon" href="http://fedoraproject.org/favicon.ico" />
      <title>Fedora L10n fr statistiques</title>
      <script src="sorttable.js"></script>
  </head>
  <body>
        <div id=headcontent>
            <div id=lefttop>
                <a href="https://fedoraproject.org"> <img src='https://fedoraproject.org/static/images/fedora-logo.png' alt='Fedora Project'> </a>
            </div>
            <div id=righttop>
                <h1>Traduction de Fedora</h1>
                <h2>Statistiques de la langue française</h2>
            </div>
        </div>
        <hr/>

        <div id="container">
'''
html_footer = u'''
    </div>
    <footer id="info">
                  <p>Code disponible sur <a href="https://gitorious.org/tiny-scripts/transifex/">gitorious.org/tiny-scripts/transifex/</a>. Design inspiré par <a href="http://keys.fedoraproject.org">keys.fedoraproject.org</a>. Attention, service en test. Données récupérées sur un <a href="https://wip.fedora-fr.org/p/traduction/source/tree/master/">miroir de nos traductions</a>.</p>
    </footer>
  </body>
</html>'''

''' This function get the local metadata file in order to generate the
HTML page '''


if not os.path.exists(METADATA) or os.path.getctime(METADATA) + TIMEOUT <  time.time():
	urllib.urlretrieve(METADATA_URL, METADATA)

fd = open(METADATA)
data = json.load(fd)
fd.close()

if os.path.exists(PRIORITY):
	fd = open(PRIORITY)
	priority_list = json.load(fd)
	fd.close()
else:
	priority_list = []

content = html_header

content += u'<h4>Type de projets</h4>'
content += u'<ul>'
for r in data['release']:
	content += u'<li><a href="#' + r + '"> ' + r + '</a></li>\n'
content += u'</ul>'

for r in data['release']:
	content += u'<h3 id="' + r + '">' + r + '</h3>'
	content += u"""<table class="sortable">
		<thead>
		  <tr><th>Projet</th><th>Completion (%)</th><th>Relu (%)</th><th>Dernière mise à jour</th></tr>
		</thead>
		<tbody>\n"""

	for p in data['release'][r]:
		if p['name'] in priority_list:
			priority = priority_list[p['name']]
		else:
			priority = 'default'
		content += u'\t\t<tr class="' + priority + '"><td><a href="https://www.transifex.com/projects/p/' + p['name'] + '"> ' + p['name'] + '</a></td>'
		content +=        u'<td>' + str(p['stats']['completed']) + '</td>'
		content +=         u'<td>' + str(p['stats']['reviewed_percentage']) + '</td>'
		date = p['stats']['last_updated']
		content +=         u'<td>' + time.strftime("%F", time.localtime(date)) + '</td></tr>\n'

	content += u'''
	</tbody>
	</table>\n'''

if 'generated' in data:
	content += u'<br/><p id="maj">Mise à jour le ' + strftime("%d %b %Y %H:%M", localtime(calendar.timegm(strptime(data['generated'], '%Y-%m-%d %H:%M:%S'))))  + u'</p>'

content += html_footer
print "Content-type: text/html\n\n"
print content.encode('utf-8')

