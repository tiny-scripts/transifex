#!/usr/bin/python
#TODO:
# - write doc
# - should add new files on the repo

try:
    import ConfigParser
except ImportError:
    print "ConfigParser module not found"

import pycurl, json, cStringIO, time, calendar, fileinput
import argparse, re, os
from os.path import expanduser

# Global variables
API_URL="https://www.transifex.com/api/2/project/"
PROJECT_URL="https://www.transifex.com/projects/p/"
METADATA='metadata'

release = ['fedora-websites', 'fedora-docs', 'fedora-main', 'fedora-upstream-projects']

''' Get Transifex username and password '''
config = ConfigParser.RawConfigParser()
config.read(expanduser('~')+'/.transifexrc')
txuser = config.get('https://www.transifex.com', 'username')
txpass = config.get('https://www.transifex.com', 'password')

''' return the json data from the url'''
def getJson(url):
    result = cStringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(pycurl.URL, API_URL + url)
    c.setopt(pycurl.HTTPHEADER, ['Accept: application/json'])
    c.setopt(pycurl.USERPWD, txuser + ":" + txpass)
    c.setopt(pycurl.WRITEFUNCTION, result.write)
    c.perform()
    c.close()
    return result.getvalue()

''' Init repos '''
def init_repos():
    curr_dir = os.getcwd()
    for r in release:
        print 'Processing ' + r

        release_j = json.loads(getJson('fedora/release/' + r + '/'))

        ''' Get the list of all projects under this release'''
        projects = []
        project_dict = dict()
        for p in release_j["resources"]:
            name = p["project"]["slug"]
            if not name in projects:
                projects.append(name)


        ''' Save the projects name for further processing '''
        project_dict = []
        for p in projects:
            project_dict.append({'name':p, 'stats':''})
        if os.path.exists(METADATA):
            f = open(METADATA, 'r')
            try:
                content = json.loads(f.read())
            except ValueError:
                print "Invalid file, erasing"
                content = {'release':dict()}
                pass
            f.close()
        else:
            content = {'release':dict()}
        f = open(METADATA,'w')
        content['release'][r] = project_dict
        f.write(json.dumps(content))
        f.close()

        ''' Init the release folder'''
        if not os.path.exists(r):
            os.makedirs(r)

        os.chdir(r)

        ''' Init the tx repo'''
        if not os.path.exists('.tx'):
            os.makedirs('.tx')
        f = open('.tx/config', 'w')
        f.write(
'''[main]
host = https://www.transifex.com

''')
        f.close()

        ''' tx set projects
            we don't directly add the release as it only contain
            specific resources. I want them all
        '''
        for p in projects:
            os.system('/usr/bin/tx set --auto-remote ' + PROJECT_URL + p)

        os.chdir(curr_dir)

    print "All repos set up"

def update_status(lang):
    print "Checking project status"
    if not os.path.exists(METADATA):
        print "Please run with --init first"
        return

    #TODO: ok to save after each project?
    f = open(METADATA, 'r')
    content = json.loads(f.read())
    f.close()

    # First, save the UTC creation date
    content['generated'] = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())

    for r in content['release']:
        for p in content['release'][r]:
            print "Processing " + p['name']
            stats = {'reviewed_percentage':0, 'completed':0, 'last_updated':''}

            ''' Get the resources attached to the project
                Then get the stats of each resources to sum up, language
                specific.
                Finally save the results (% completion, % proofread,
                last commiter, time of last update)
            '''
            url = p['name'] + '/?details'
            p_detail = json.loads(getJson(url.encode('ascii','ignore')))

            nbr_resources = 0
            last_update = 0
            for s in p_detail['resources']:
                url = p['name'] + '/resource/' + s['slug'] + '/?details/'
                detail = json.loads(getJson(url.encode('ascii','ignore')))
                try:
                    if detail['accept_translations'] is not 'True':
                        continue
                except KeyError:
                    pass

                nbr_resources +=1
                url = p['name'] + '/resource/' + s['slug'] + '/stats/'
                stat = json.loads(getJson(url.encode('ascii','ignore')))
                stats['reviewed_percentage'] += int(stat[lang]['reviewed_percentage'].split('%')[0])
                stats['completed'] += int(stat[lang]['completed'].split('%')[0])

                tuple_time = time.strptime(stat[lang]["last_update"], "%Y-%m-%d %H:%M:%S")
                if calendar.timegm(tuple_time) > last_update:
                    last_update = calendar.timegm(tuple_time)

            stats['completed'] /= nbr_resources
            stats['reviewed_percentage'] /= nbr_resources
            stats['last_updated'] = last_update
            for name in content['release'][r]:
                if name['name'] == p['name']:
                    name['stats'] = stats

            f = open(METADATA,'w')
            f.write(json.dumps(content))
            f.close()

    print 'done'


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Fedora L10n helper - language code specific. Depends on the transifex-client. You also need your transifex.com credentials at ~/.transifexrc. -- See `tx init`')
    parser.add_argument('--init', action='store_true', help='Init the Transifex repositories (for each Fedora HUB releases)')
    parser.add_argument('-l', '--lang', action='store', help='Parse the provided language code. Needed to get the stats')

    args = parser.parse_args()

    if args.init:
        init_repos()
        print "Init Done, you might want to pull translations now and then create the stats"
    else:
        if not args.lang:
            print "Finished, have you missed the --lang argument?"
        else:
            update_status(args.lang)

